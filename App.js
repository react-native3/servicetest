import {createAppContainer} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack'

// Navigators
import bottomTabStackNavigation from './navigation/bottomTabNavigation/BottomTabNavigation';
import loginNavigator from './navigation/loginNavigation/LoginNavigation';
import splashNavigator from './navigation/splashNavigation/SplashNavigation';
import userInfoNavigation from './navigation/userInfoNavigation/UserInfoNavigation';

const startAppNavigator = createStackNavigator({
    splash : splashNavigator,
    login : loginNavigator,
    tabStack : bottomTabStackNavigation,
    userInfo : userInfoNavigation
});

const startAppContainer = createAppContainer(startAppNavigator);

export default startAppContainer;