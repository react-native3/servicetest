import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { TouchableOpacity, Text, StyleSheet } from 'react-native';
import { BaseColours } from '../../../css/colours';

class SuccessButton extends Component {
    render() {
        const { text, onPress} = this.props;
		return (
		  <TouchableOpacity style={styles.buttonStyle} onPress={() => onPress()}>
			 <Text style={styles.textStyle}>{text}</Text>
		  </TouchableOpacity>
		);
    }
}

SuccessButton.prototypes = {
    text: PropTypes.string.isRequired,
    onPress: PropTypes.func.isRequired
}

const styles = StyleSheet.create({
    textStyle: {
      fontSize:12,
      color: '#ffffff',
      textAlign: 'center'
    },
    buttonStyle: {
      marginTop:10,
      padding:10,
      backgroundColor: BaseColours.success,
      borderRadius:50
    }
});

export default SuccessButton;