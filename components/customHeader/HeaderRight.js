import React, { Component } from 'react';
import { View, Text } from 'react-native';
import AntIcon from 'react-native-vector-icons/AntDesign';
import AsyncStorage from '@react-native-community/async-storage';

export default class HeaderRight extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  logout = (navigation) => {
    console.log("test");
    AsyncStorage.removeItem('user');
    navigation.navigate('login');
  }

  render() {
    const {navigation} = this.props;
    return (
      <View style={{marginRight : 10}}>
        <AntIcon.Button name = "logout" onPress={() => this.logout(navigation)}> Çıkış </AntIcon.Button>
      </View>
    );
  }
}
