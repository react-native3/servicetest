import React, { Component } from 'react';
import { View, Text,TouchableOpacity,StyleSheet } from 'react-native';
import FastImage from 'react-native-fast-image';
import Icon from 'react-native-vector-icons/AntDesign'
import EIcon from 'react-native-vector-icons/Entypo'
import { BaseColours } from '../../../css/colours';

export default class Row extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    const {item,onPress} = this.props;
    return (
      <TouchableOpacity onPress = {onPress} style={{marginBottom:10,borderRadius : 20,backgroundColor : BaseColours.warning,overflow : 'hidden'}}>
          <View>
            <Text style={{fontSize : 20,textAlign : 'center',textAlignVertical : 'center',marginBottom : 10,backgroundColor : BaseColours.primary,padding : 5,borderTopLeftRadius : 20,borderTopRightRadius : 20,overflow : 'hidden',color : BaseColours.white}}>{item.productName}</Text>
            <FastImage
                style={{ width: '100%', height: 200 }}
                source={{
                    uri: item.images[0].normal
                }}
                resizeMode={FastImage.resizeMode.contain}
            />
            <View style={{flexDirection : 'row',justifyContent:'space-between',borderBottomLeftRadius:20,borderBottomRightRadius:20,backgroundColor:BaseColours.white,padding : 5}}>
                <View style={{width:75,flexDirection: 'row',justifyContent:'space-between'}}>
                    <TouchableOpacity>
                        {
                            item.productName === "MacBook" && <Icon name = "heart" size={25}/> 
                        }
                        {
                            item.productName !== "MacBook" && <Icon name = "hearto" size={25}/> 
                        }
                    </TouchableOpacity>
                    <TouchableOpacity>
                        <Icon name = "like2" size={25}/>
                    </TouchableOpacity>
                    <TouchableOpacity>
                        <Icon name = "comment" size={25}/>
                    </TouchableOpacity>
                </View>
                <View>
                    <TouchableOpacity>
                        <EIcon name = "bookmark" size={25}/>
                    </TouchableOpacity>
                </View>
            </View>
          </View>
      </TouchableOpacity>
    );
  }
}
