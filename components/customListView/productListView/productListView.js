import React from 'react';
import { View, FlatList, StyleSheet } from 'react-native';
import ProductRow from './productRow';

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
});

const ProductListView = ({ itemList,onPresDetails }) => (
    <View style={styles.container}>
        <FlatList
                data={itemList}
                renderItem={({ item }) => <ProductRow
                    title={item.productName}
                    description={item.description}
                    image_url = {item.images[0].normal}
                    onPressDetails = {(item) => onPresDetails(item)}
                    item = {item}
                />}
                keyExtractor={(item) => item.productId}
        />
    </View>
);

export default ProductListView;