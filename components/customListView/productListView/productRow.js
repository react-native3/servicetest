import React from 'react';
import { View, Text, StyleSheet, Image } from 'react-native';
import PrimaryButton from '../../buttons/primary/PrimaryButton';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'row',
        padding: 10,
        marginLeft:16,
        marginRight:16,
        marginTop: 8,
        marginBottom: 8,
        borderRadius: 20,
        backgroundColor: '#FFF',
        elevation: 2,
    },
    title: {
        fontSize: 16,
        color: '#000',
    },
    container_text: {
        flex: 3,
        flexDirection: 'column',
        marginLeft: 12,
        justifyContent: 'center',
    },
    description: {
        fontSize: 11,
        fontStyle: 'italic',
    },photo : {
        height: 60,
        width: 60,
        borderRadius: 10,
        overflow: "hidden"
    }
});

const ProductRow = ({ title, description,image_url,onPressDetails,item }) => (
    <View style={styles.container}>
        <Image source={{ uri: image_url }} style={styles.photo} resizeMode='stretch'/>
        <View style={styles.container_text}>
            <Text style={styles.title}>
                {title}
            </Text>
            <Text style={styles.description} numberOfLines={3}>
                {description}
            </Text>
            <PrimaryButton text="Detaylar" onPress={()=>{onPressDetails(item)}}/>
        </View>
    </View>
);

export default ProductRow;