export const BaseColours = {
    primary : 'rgb(48, 125, 245)',
    success : 'rgb(80, 160, 80)',
    danger : 'rgb(203,68,74)',
    warning : 'rgb(246,194,68)',
    secondary : 'rgb(110,117,124)',
    white : '#ffffff'
}