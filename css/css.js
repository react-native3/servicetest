import {StyleSheet} from 'react-native';

export const utilStyle = StyleSheet.create({
    container : {
        backgroundColor : '#E1F5FE',
        flex : 1
    },scrollView : {
        padding : 10
    },buttonStyle : {
        marginTop : 20,
        textAlign : 'center',
        width : '100%',
        backgroundColor : 'purple',
        textAlignVertical : 'center',
        justifyContent : 'center',
        color : 'white',
        borderRadius:100,
        lineHeight: 40
    }
});