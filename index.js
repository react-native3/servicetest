/**
 * @format
 */

import {AppRegistry} from 'react-native';
import {name as appName} from './app.json';

import startAppContainer from './App';

AppRegistry.registerComponent(appName, () => startAppContainer);
