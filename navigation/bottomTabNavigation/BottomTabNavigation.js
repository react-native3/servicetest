import React, { Component } from 'react';
import { createBottomTabNavigator } from 'react-navigation-tabs';
import Icon from 'react-native-vector-icons/AntDesign';
import FontistoIcon from 'react-native-vector-icons/Fontisto';

import productStack from "./productNavigation/ProductStackNavigation";
import mapStack from "./mapsNavigation/MapStackNavigation";
import cameraStack from './cameraNavigation/CameraStackNavigation';


const bottomTabStackNavigator = createBottomTabNavigator({
    TabProduct : {
        screen : productStack,
        navigationOptions : {
            tabBarLabel : "Ürünler",
            tabBarIcon : <Icon name="shoppingcart" size={30}/>
        }
    },TabMap : {
        screen : mapStack,
        navigationOptions : {
            tabBarLabel : "Harita",
            tabBarIcon : <FontistoIcon name="world-o" size = {30}/>
        }
    },TabCam : {
        screen : cameraStack,
        navigationOptions : {
            tabBarLabel : "Kamera",
            tabBarIcon : <Icon name="camerao" size = {30}/>
        }
    }
},{
    initialRouteName : "TabProduct",
    
});

const bottomTabStackNavigation = {
    screen : bottomTabStackNavigator,
    navigationOptions : {
        header : null
    }
};

export default bottomTabStackNavigation;