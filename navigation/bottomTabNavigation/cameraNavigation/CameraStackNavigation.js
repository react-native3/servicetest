import {createAppContainer} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';
import cameraNavigator from './pages/CameraNavigator';


const cameraStackNavigator = createStackNavigator({
    map : cameraNavigator
});

const cameraStack = createAppContainer(cameraStackNavigator);

export default cameraStack;