import {createAppContainer} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';
import mapNavigator from './pages/MapNavigation';


const mapStackNavigator = createStackNavigator({
    map : mapNavigator
});

const mapStack = createAppContainer(mapStackNavigator);

export default mapStack;