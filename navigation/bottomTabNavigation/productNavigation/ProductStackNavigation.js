import {createAppContainer} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack'
import productNavigator from './pages/ProductNavigation';
import productDetailNavigator from './pages/ProductDetailNavigation';


const productStackNavigator = createStackNavigator({
    product : productNavigator,
    productDetail : productDetailNavigator
},{
    initialRouteName : "product"
});

const productStack = createAppContainer(productStackNavigator);

export default productStack;