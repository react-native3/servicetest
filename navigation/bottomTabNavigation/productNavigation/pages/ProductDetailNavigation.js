import React, { Component } from 'react';

import HeaderRight from '../../../../components/customHeader/HeaderRight';
import ProductDetail from '../../../../pages/ProductDetail';


const productDetailNavigator = {
    screen : ProductDetail,
    navigationOptions : ({ navigation }) => {
        const { params } = navigation.state;
        return {
          title: params.product.productName ? params.product.productName: 'Default Screen Title',
          headerRight: () => (<HeaderRight/>),
        }
    }
};

export default productDetailNavigator;