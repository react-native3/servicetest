import React, { Component } from 'react';

import HeaderRight from '../../../../components/customHeader/HeaderRight';
import Product from '../../../../pages/Product';

const productNavigator = {
    screen : Product,
    navigationOptions: ({navigation}) => {
        return {
            title : 'Ürünler',
            gesturesEnabled: false,
            headerLeft : null,
            headerRight: () => (
                <HeaderRight navigation={navigation}/>
            )
        }
    }
};

export default productNavigator;