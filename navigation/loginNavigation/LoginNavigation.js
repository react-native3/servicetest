import Login from "../../pages/Login";

const loginNavigator = {
    screen : Login,
    navigationOptions: {
        gesturesEnabled: false,
        headerLeft : null,
        title : 'Giriş Yapınız'
    }
};

export default loginNavigator;