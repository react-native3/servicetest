import Splash from "../../pages/Splash";


const splashNavigator = {
    screen : Splash,
    navigationOptions: {
        gesturesEnabled: false,
        header: null
    }
};

export default splashNavigator;