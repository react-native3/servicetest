import React, { Component } from 'react';
import UserInfo from "../../pages/UserInfo";
import HeaderRight from "../../components/customHeader/HeaderRight";


const userInfoNavigation = {
    screen : UserInfo,
    navigationOptions: ({navigation}) => {
        return {
            title: 'Kullanıcı Bilgileri',
            gestureEnabled : false,
            headerLeft : null,
            headerRight : () => (
                <HeaderRight />
            )
        }
    }
}

export default userInfoNavigation;