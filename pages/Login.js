import React, { Component } from 'react';
import { SafeAreaView,TextInput,ScrollView,TouchableOpacity ,Text} from 'react-native';
import { Hoshi } from 'react-native-textinput-effects';
import { utilStyle } from '../css/css';
import axios from 'axios';
import AsyncStorage from '@react-native-community/async-storage';

import {
  GoogleSignin,
  GoogleSigninButton,
  statusCodes,
} from '@react-native-community/google-signin';

export default class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      userEmail:'',
      userPass:'',
    };
  }


  componentDidMount(){
    GoogleSignin.configure({
      scopes: ['https://www.googleapis.com/auth/drive.readonly'], // what API you want to access on behalf of the user, default is email and profile
      webClientId: '119153465836-5gqftku3fhh55id3sf8rap2j26rmfmjf.apps.googleusercontent.com', // client ID of type WEB for your server (needed to verify user ID and offline access)
      /*offlineAccess: true, // if you want to access Google API on behalf of the user FROM YOUR SERVER
      hostedDomain: '', // specifies a hosted domain restriction
      loginHint: '', // [iOS] The user's ID, or email address, to be prefilled in the authentication UI if possible. [See docs here](https://developers.google.com/identity/sign-in/ios/api/interface_g_i_d_sign_in.html#a0a68c7504c31ab0b728432565f6e33fd)
      forceConsentPrompt: true, // [Android] if you want to show the authorization prompt at each login.
      accountName: 'karadenizfaruk28', // [Android] specifies an account name on the device that should be used
      iosClientId: '119153465836-0bemc2hgq0t1ln47ijepfg8ahf73rj9j.apps.googleusercontent.com', // [iOS] optional, if you want to specify the client ID of type iOS (otherwise, it is taken from GoogleService-Info.plist)
    */});
  }

  callLogin = () => {
    const mail = this.state.userEmail;
    const pass = this.state.userPass;

    const variables = {
      ref:'5380f5dbcc3b1021f93ab24c3a1aac24',
      userEmail : mail,
      userPass : pass,
      face:'no'
    };
    const url = "https://www.jsonbulut.com/json/userLogin.php";
    axios.get(
      url,{params:variables}    
    ).then(response => {
      if(response != null && response.data != null && response.data.user != null && response.data.user[0].durum){
          AsyncStorage.setItem('user',JSON.stringify(response.data.user[0].bilgiler));
          this.props.navigation.navigate('product');
      }         
    }).catch(error => {
        console.log(error);
    });
  }

  _signIn = async () => {
    //Prompts a modal to let the user sign in into your application.
    try {
      await GoogleSignin.hasPlayServices({
        //Check if device has Google Play Services installed.
        //Always resolves to true on iOS.
        showPlayServicesUpdateDialog: true,
      });
      const userInfo = await GoogleSignin.signIn();
      console.log('User Info --> ', userInfo);
      AsyncStorage.setItem('userGoogle',JSON.stringify(userInfo));
      this.props.navigation.navigate('userInfo');
    } catch (error) {
      console.log('Message', error.message);
      if (error.code === statusCodes.SIGN_IN_CANCELLED) {
        console.log('User Cancelled the Login Flow');
      } else if (error.code === statusCodes.IN_PROGRESS) {
        console.log('Signing In');
      } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
        console.log('Play Services Not Available or Outdated');
      } else {
        console.log('Some Other Error Happened');
      }
    }
  };

  render() {
    return (
      <SafeAreaView style = {utilStyle.container}>
        <ScrollView style={utilStyle.scrollView}>
          <Hoshi onChangeText={(txt)=> this.setState({userEmail:txt})} keyboardType="email-address" label="Mail Adresini Giriniz" autoCapitalize="none"/>
          <Hoshi onChangeText={(txt)=> this.setState({userPass:txt})} label="Şifre Giriniz" secureTextEntry/>
          <TouchableOpacity onPress={this.callLogin}>
             <Text style={utilStyle.buttonStyle}>Giriş Yap</Text>
          </TouchableOpacity>
          <GoogleSigninButton
              style={{ width: '100%', height: 48 }}
              size={GoogleSigninButton.Size.Standard}
              color={GoogleSigninButton.Color.Light}
              onPress={this._signIn}
              disabled={this.state.isSigninInProgress} />
        </ScrollView>
      </SafeAreaView>
    );
  }
}
