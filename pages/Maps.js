import React, { Component } from 'react';
import { View, Text,SafeAreaView } from 'react-native';
import MapView from 'react-native-maps';


export const getCurrentLocation = () => {
    return new Promise((resolve, reject) => {
      navigator.geolocation.getCurrentPosition(position => resolve(position), e => reject(e));
    });
};

export default class Maps extends Component {
  constructor(props) {
    super(props);
    this.state = {
        region : {

        }
    };
    //AIzaSyDZKWjEfU1sNXtmAZZkn7xAZ1fRAAcUJ_w
  }

  marker =  [
    { coordinates:{latitude: 40.9521632, longitude: 29.0944064}, title: "Hatay Restoran" },
    { coordinates:{latitude: 40.4521632, longitude: 28.0944064}, title: "MMM Migros" },
    { coordinates:{latitude: 40.7521632, longitude: 27.0944064}, title: "Adıyaman Çiğköfte" },
  ]

  componentDidMount() {
      getCurrentLocation().then(position => {
          if(position){
                this.setState({region:position.region});
          }
      });
  }

  render() {
    return (
      <SafeAreaView>
        <MapView 
            style={{width:'100%',height:'100%'}}
            initialRegion={{
                latitude: 40.9482696,
                longitude: 29.1034722,
                latitudeDelta: 0.0922,
                longitudeDelta: 0.0421,
            }}
            //followsUserLocation = {true}
            showsMyLocationButton = {true}
            //showsUserLocation = {true}
            showsTraffic = {true}
        >
            {this.marker.map(item => (
                <MapView.Marker
                    coordinate={item.coordinates}
                    title={item.title}
                    description={item.title}
                    onPress = {() => {
                        console.log("detail marker clicked");
                        console.log("marker title : " + item.title);
                    }}
                />
            ))}
        </MapView>
      </SafeAreaView>
    );
  }
}
