import React, { Component } from 'react';
import { BackHandler,SafeAreaView,ScrollView,Alert,TouchableOpacity,Text,View,Dimensions } from 'react-native';
import { utilStyle } from '../css/css';
import axios from 'axios';
import ProductListView from '../components/customListView/productListView/productListView';
import { FlatList } from 'react-native-gesture-handler';
import Row from '../components/customListView/lesson/Row';
import * as Progress from 'react-native-progress';

const w = Dimensions.get('window').width; 
const h = Dimensions.get('window').height; 

export default class Product extends Component {
  constructor(props) {
    super(props);
    this.state = {
      products : [],
      loadStatus : true
    };
  }

  componentDidMount(){
      BackHandler.addEventListener('hardwareBackPress',this.androidBackButtonPress);
      const url = "https://www.jsonbulut.com/json/product.php?ref=5380f5dbcc3b1021f93ab24c3a1aac24&start=0";
      axios.get(url).then(res => {
        this.interval = setInterval(async () => {
          this.setState({ products: res.data.Products[0].bilgiler ,loadStatus : false});
          clearInterval(this.interval);
      }, 3000);
      });
  }

  componentWillUnmount(){
      BackHandler.removeEventListener('hardwareBackPress',this.androidBackButtonPress);
  }

  androidBackButtonPress = () => {
    this.props.navigation.goBack(null);
    return true;
  }

  fncDetail = (item) => {
    this.props.navigation.navigate('productDetail',{
      product : item
    })
  }

  render() {
    /*return (
      <SafeAreaView style={utilStyle.container}>
        <ProductListView itemList = {this.state.products} onPresDetails = {(productDetail) => this.props.navigation.navigate('productDetail',{
          product : productDetail
        })}/>
      </SafeAreaView>
    );*/
    return (
      <SafeAreaView style={utilStyle.container}>
        {this.state.loadStatus &&
          <View style={{position:'absolute',justifyContent : 'center',alignItems: 'center',top:(h/2) - 200,left:(w/2) - 50,zIndex:10}}>
              <Progress.Circle size={100} indeterminate={true} />
          </View>
        }
        <FlatList
          style={{padding : 10}}
          data = {this.state.products}
          keyExtractor = {(item) => item.productId}
          renderItem = {({ item }) => <Row item = {item} onPress = {() => this.fncDetail(item)}/>}
        />
      </SafeAreaView>
    );
  }
}