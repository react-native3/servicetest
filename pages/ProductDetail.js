import React, { Component } from 'react';
import { SafeAreaView,ScrollView,TouchableOpacity,Modal,TouchableWithoutFeedback,Text } from 'react-native';
import { utilStyle } from '../css/css';
import FastImage from 'react-native-fast-image';
import ImageViewer from 'react-native-image-zoom-viewer';
import { BaseColours } from '../css/colours';
import { WebView } from 'react-native-webview';


export default class ProductDetail extends Component {
  imageArray = [];
  constructor(props) {
    super(props);
    this.state = {
      product : this.props.navigation.getParam('product',null),
      imageStatu : false
    };
    this.state.product.images.map((item,index) => {
      this.imageArray.push({ url : item.normal});
    });
    this.props.navigation.setParams({ title: this.state.product.productName });
  }

  render() {
    const  productItem  = this.state.product;
    return (
      <SafeAreaView style={utilStyle.container}>
        <ScrollView>
            <Modal visible={this.state.imageStatu} onDismiss={() => this.setState({imageStatu : false})} transparent={true}>
                <TouchableWithoutFeedback onPress = {() => this.setState({imageStatu : false})}>
                  <Text style = {{position:'absolute',color : BaseColours.white,fontSize:20,borderWidth:1,borderRadius:5,borderColor:BaseColours.white,overflow:'hidden',margin:30,padding:5,zIndex:1,top:30,left:10}}>Kapat</Text>
                </TouchableWithoutFeedback>
                <ImageViewer 
                  imageUrls = {this.imageArray}
                  enableSwipeDown = {true}
                  onSwipeDown = {() => this.setState({imageStatu : false})}
                  saveToLocalByLongPress = {false}
                  enablePreload = {true}
                />
            </Modal>
            <TouchableOpacity onPress = {() => this.setState({imageStatu : true})} style={{margin : 10}}>
              <FastImage
                  style={{width : '100%',height:200}}
                  source = {{ uri : productItem.images[0].normal }}
                  resizeMode = {FastImage.resizeMode.cover}
              />
            </TouchableOpacity>
            <Text>{}</Text>
            <WebView
                style={{ width: '100%' , height:400 }}
                originWhitelist={['*']}  
                source={{ html: `<!DOCTYPE html>
                <html>
                <head>
                  <meta charset="utf-8">
                  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=1.0, minimum-scale=1.0, maximum-scale=1.0">
                  <meta http-equiv="X-UA-Compatible" content="IE=edge">
                  <title></title>
                  <link rel="stylesheet" href="">
                </head>
                <body>
                  ${productItem.description}
                </body>
                </html>` }}
              />
        </ScrollView> 
      </SafeAreaView>
    );
  }
}
