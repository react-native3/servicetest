import React, { Component } from 'react';
import { View, Text,Image,Dimensions } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';

const w = Dimensions.get('window').width; 

const h = Dimensions.get('window').height; 

export default class Splash extends Component {

    componentDidMount(){
        this.interval = setInterval(async () => {
            const use = await AsyncStorage.getItem("user");
            clearInterval(this.interval);
            if(use === null ){
                this.props.navigation.navigate("login");
            }else {
                this.props.navigation.navigate("product");
            }
        }, 3000);
   }

    constructor(props) {
        super(props);
        this.state = {
        };
    }

  render() {
    return (
      <View style={{flex : 1}}>
        <Image source={require('./../assets/splash-screen.jpeg')}  style={{width:w,height:h}}/>
      </View>
    );
  }
}
