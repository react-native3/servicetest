import React, { Component } from 'react';
import { View, Text } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';

export default class UserInfo extends Component {
  constructor(props) {
    super(props);
    this.state = {
        userGoogle:{}
    };
  }

  async componentDidMount(){
    const user = await AsyncStorage.getItem("userGoogle");
    console.log(user);
    this.setState({userGoogle : user})
  }

  render() {
    return (
      <View>
        <Text> {this.state.userGoogle && this.state.userGoogle.user && this.state.userGoogle.user.email} </Text>
      </View>
    );
  }
}
